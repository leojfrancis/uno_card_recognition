from scipy.spatial import distance
from typing import List, Tuple
import cv2 as cv
import numpy as np
from utils import euclidian_distance, show
from functools import partial


def order_points(pts):
    '''Rearrange coordinates to order:
      top-left, top-right, bottom-right, bottom-left'''
    rect = np.zeros((4, 2), dtype='float32')
    pts = np.array(pts)
    s = pts.sum(axis=1)
    # Top-left point will have the smallest sum.
    rect[0] = pts[np.argmin(s)]
    # Bottom-right point will have the largest sum.
    rect[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis=1)
    # Top-right point will have the smallest difference.
    rect[1] = pts[np.argmin(diff)]
    # Bottom-left will have the largest difference.
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return rect.astype('int').tolist()


def find_dest(pts):
    (tl, tr, br, bl) = pts
    # Finding the maximum width.
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # Finding the maximum height.
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # Final destination co-ordinates.
    destination_corners = [[0, 0], [maxWidth, 0],
                           [maxWidth, maxHeight], [0, maxHeight]]

    return order_points(destination_corners)


def get_target_image(img, debug=False) -> Tuple[cv.typing.MatLike, bool]:
    # Resize image to workable size when stand alone
    # dim_limit = 1080
    # max_dim = max(img.shape)
    # if max_dim > dim_limit:
    #     resize_scale = dim_limit / max_dim
    #     img = cv.resize(img, None, fx=resize_scale, fy=resize_scale)
    # Create a copy of resized original image for later use
    orig_img = img.copy()

    # Repeated Closing operation to remove text from the document.
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5, 5))
    img = cv.morphologyEx(img, cv.MORPH_CLOSE, kernel, iterations=20)

    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (9, 9), 2)
    _, thresh = cv.threshold(blur, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
    if debug:
        show(blur)
    if debug:
        show(thresh)
    # exit(0)
    # Edge Detection.
    canny = cv.Canny(blur, 50, 150, apertureSize=3)
    canny = cv.dilate(canny, kernel)
    if debug:
        show(canny)

    # Finding contours for the detected edges.
    contours, hierarchy = cv.findContours(
        canny, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    # Keeping only the largest detected contour.
    # key = partial(cv.arcLength, closed=False)
    key = cv.contourArea
    page = sorted(contours, key=key, reverse=True)[:1]

    # Detecting Edges through Contour approximation.
    # Loop over the contours.
    if len(page) == 0:
        return orig_img, False
    for c in page:
        # Approximate the contour.
        epsilon = 0.02 * cv.arcLength(c, True)
        corners = cv.approxPolyDP(c, epsilon, True)
        # If our approximated contour has four points.
        if debug:
            print(cv.contourArea(c))
        if cv.contourArea(c) < 1500:
            return orig_img, False
        if len(corners) == 4:
            break

    con = np.zeros_like(img)
    cv.drawContours(con, c, -1, (0, 255, 255), 3)
    cv.drawContours(con, corners, -1, (0, 255, 0), 10)
    if debug:
        show(con)

    # Sorting the corners and converting them to desired shape.
    corners = sorted(np.concatenate(corners).tolist())
    # For 4 corner points being detected.
    corners = order_points(corners)

    destination_corners = find_dest(corners)

    # Getting the homography.
    M = cv.getPerspectiveTransform(np.float32(
        corners), np.float32(destination_corners))
    # Perspective transform using homography.
    final = cv.warpPerspective(orig_img, M, (destination_corners[2][0], destination_corners[2][1]),
                               flags=cv.INTER_LINEAR)

    return cv.resize(final, (264, 264)), True
