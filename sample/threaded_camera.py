from threading import Thread
import cv2, time

class ThreadedCamera(object):
    """
    Class to use threads to perform parellel computation by capturing frames and process data 
    """
    def __init__(self, src=0):
        self.capture = cv2.VideoCapture(src)
        self.capture.set(cv2.CAP_PROP_BUFFERSIZE, 2)
       
        # FPS = 1/X
        # X = desired FPS
        self.FPS = 1/30
        self.FPS_MS = int(self.FPS * 1000)
        
        # Start frame retrieval thread
        self.thread = Thread(target=self.update, args=())
        self.thread.daemon = True
        self.thread.start()
        
    def update(self):
        while True:
            if self.capture.isOpened():
                (self.status, self.frame) = self.capture.read()
            time.sleep(self.FPS)
            
    def frame(self):
        return(self.frame)
        cv2.imshow('frame', self.frame)
        cv2.waitKey(self.FPS_MS)