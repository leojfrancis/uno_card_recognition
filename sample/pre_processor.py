import cv2 as cv
import math
from scipy import ndimage
import numpy as np
from .utils import show


class RotationCorrector:
    def __init__(self, output_process=False):
        self.output_process = output_process

    def __call__(self, image):
        img_before = image.copy()

        img_edges = cv.Canny(img_before, 50, 150, apertureSize=3)
        lines = cv.HoughLinesP(
            img_edges,
            1,
            math.pi / 90.0,
            100,
            minLineLength=100,
            maxLineGap=5
        )
        print("Number of lines found:", len(lines))

        def get_angle(line):
            x1, y1, x2, y2 = line[0]
            return math.degrees(math.atan2(y2 - y1, x2 - x1))

        median_angle = np.median(np.array([get_angle(line) for line in lines]))
        img_rotated = ndimage.rotate(
            img_before,
            median_angle,
            cval=255,
            reshape=False
        )

        print("Angle is {}".format(median_angle))

        if self.output_process:
            show(img_rotated)

        return img_rotated


class Resizer:
    """Resizes image.

    Params
    ------
    image   is the image to be resized
    height  is the height the resized image should have. Width is changed by similar ratio.

    Returns
    -------
    Resized image
    """

    def __init__(self, height=1280, output_process=False):
        self._height = height
        self.output_process = output_process

    def __call__(self, image):
        # if image.shape[0] <= self._height: return image
        # ratio = round(self._height / image.shape[0], 3)
        # width = int(image.shape[1] * ratio)
        # dim = (width, self._height)
        resized = cv.resize(image, (self._height, self._height),
                            interpolation=cv.INTER_AREA)
        if self.output_process:
            show(resized)
        return resized


class FastDenoiser:
    """Denoises image by using the fastNlMeansDenoising method"""

    def __init__(self, strength=7, output_process=False):
        self._strength = strength
        self.output_process = output_process

    def __call__(self, image):
        temp = cv.fastNlMeansDenoising(image, h=self._strength)
        if self.output_process:
            show(temp)
        return temp


class OtsuThresholder:
    def __init__(self, thresh1=0, thresh2=255, output_process=False):
        self.output_process = output_process
        self.thresh1 = thresh1
        self.thresh2 = thresh2

    def __call__(self, image):
        image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
        T_, thresholded = cv.threshold(
            image, self.thresh1, self.thresh2, cv.THRESH_BINARY + cv.THRESH_OTSU)
        if self.output_process:
            show(thresholded)
        return thresholded


class Closer:
    def __init__(self, kernel_size=3, iterations=10, output_process=False):
        self._kernel_size = kernel_size
        self._iterations = iterations
        self.output_process = output_process

    def __call__(self, image):
        kernel = cv.getStructuringElement(
            cv.MORPH_ELLIPSE,
            (self._kernel_size, self._kernel_size)
        )
        closed = cv.morphologyEx(
            image,
            cv.MORPH_CLOSE,
            kernel,
            iterations=self._iterations
        )

        if self.output_process:
            show(closed)
        return closed


class EdgeDetector:
    def __init__(self, output_process=False):
        self.output_process = output_process

    def __call__(self, image, thresh1=50, thresh2=150, apertureSize=3):
        edges = cv.Canny(image, thresh1, thresh2, apertureSize=apertureSize)
        if self.output_process:
            show(edges)
        return edges
