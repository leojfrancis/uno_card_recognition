import utils
import sample.target_detection_contour as target_detection_contour
import target_detection_hough
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import os
from utils import color_string, find_color, show


MIN_MATCH_COUNT = 10

def match_features(template, image_of_intrest):
    # Initiate SIFT detector
    sift = cv.SIFT_create()
    
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(template,None)
    kp2, des2 = sift.detectAndCompute(image_of_intrest,None)
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    
    flann = cv.FlannBasedMatcher(index_params, search_params)
    
    matches = flann.knnMatch(des1,des2,k=2)
    
    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        # if m.distance < 0.7*n.distance:
            good.append(m)
            
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
        
        M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()
        
        h,w = template.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv.perspectiveTransform(pts,M)
        
        image_of_intrest = cv.polylines(image_of_intrest,[np.int32(dst)],True,255,3, cv.LINE_AA)    
    else:
        print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
        matchesMask = None
    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
    singlePointColor = None,
    matchesMask = matchesMask, # draw only inliers
    flags = 2)
    
    img3 = cv.drawMatches(template,kp1,image_of_intrest,kp2,good,None,**draw_params)
    show(img3)

def template_matching(template, img):
    img2 = img.copy()
    w, h = template.shape[::-1]
    
    # All the 6 methods for comparison in a list
    methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR',
    'cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']
    
    for meth in methods:
        img = img2.copy()
        method = eval(meth)
    
        # Apply template Matching
        res = cv.matchTemplate(img,template,method)
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
        
        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        
        cv.rectangle(img,top_left, bottom_right, 255, 2)
        
        plt.subplot(121),plt.imshow(res,cmap = 'gray')
        plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
        plt.subplot(122),plt.imshow(img,cmap = 'gray')
        plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
        plt.suptitle(meth)
        
        plt.show()

def detect_predominant_color(image):
    # Convert BGR to RGB
    image_hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    
    # Calculate color histograms
    hist_hue = cv.calcHist([image_hsv], [0], None, [180], [0, 180])  # Hue channel histogram
    
    # Find peak in histogram
    predominant_hue = np.argmax(hist_hue)
    
    # Define color ranges for red, green, blue, and yellow
    red_range = ([0, 100, 100], [10, 255, 255])  # Red hue range in HSV
    green_range = ([50, 100, 100], [70, 255, 255])  # Green hue range in HSV
    blue_range = ([110, 100, 100], [130, 255, 255])  # Blue hue range in HSV
    yellow_range = ([20, 100, 100], [40, 255, 255])  # Yellow hue range in HSV
    
    # Check if predominant hue falls within defined ranges
    if red_range[0][0] <= predominant_hue <= red_range[1][0]:
        print("Predominant color: Red")
    elif green_range[0][0] <= predominant_hue <= green_range[1][0]:
        print("Predominant color: Green")
    elif blue_range[0][0] <= predominant_hue <= blue_range[1][0]:
        print("Predominant color: Blue")
    elif yellow_range[0][0] <= predominant_hue <= yellow_range[1][0]:
        print("Predominant color: Yellow")
    else:
        print("Unable to determine predominant color")


false_positive = 0
score = 0
fail = 0

i = 1
j = 3
kernel = cv.getStructuringElement(cv.MORPH_CROSS, (3, 3))
if __name__ == '__main__':
    for i in range(10):
        for j in range(70,80):
            # image = utils.get_and_preprocess_image(f'./data/train/{i}/{j}.png', False)
            # target_image, detected = target_detection_contour.get_target_image(image)
            # if detected:
            #     show(target_image)
            #     target_image = cv.cvtColor(target_image, cv.COLOR_BGR2GRAY)
            #     target_image = cv.Canny(target_image, 0,150)
            #     target_image = cv.dilate(target_image, kernel)
            #     template = utils.get_and_preprocess_image(f'./data/templates/0.png', resize=False, crop=False)
            #     template = cv.Canny(template, 0,150)
            #     template = cv.Canny(template, 0,150)
            #     template = cv.dilate(template, kernel)
            #     show(template)
            #     template_matching(template, target_image)
            #     dominant_color, color_image = utils.find_color(target_image)
            #     color_string = utils.color_string(dominant_color)
            #     # show(color_image)
            #     print(f"{i} {j} {color_string}")
            #     if color_string != 'b':
            #         cv.imwrite(f'./test/{i}_{j}.png', target_image)
            #         false_positive += 1
            #     score += 1
            # else:
            #     print(f"{i} {j} false")
            #     fail += 1
    # print(score/(10*75))
    # print(false_positive/score)
        # final = cv.imread('./data/blue_0.png')
    # show(final)
            _image = utils.get_and_preprocess_image(f'./data/{i}/{j}.png')
            _number_roi, _image_roi, status = target_detection_hough.detect_target(
                _image)
            if status:
                _, img = find_color(_image_roi)
                show(_image_roi, cv_show=True, auto=1)
                # show(img, cv_show=True)
                # show(_number_roi, cv_show=True)
                print(color_string(_), i , j)
    cv.destroyAllWindows()