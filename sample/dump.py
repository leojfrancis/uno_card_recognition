# CONTOUR_MAX_AREA = 100

# image, contours = img_preprocessing('./assets/uno_green_3.jpg')
# template, _ = img_preprocessing('./assets/blank_template.jpg', blur=(1, 1))

# filtered_contours = []
# for contour in contours:
#     area = cv.contourArea(contour)
#     if area > CONTOUR_MAX_AREA:  # Adjust this threshold as needed
#         filtered_contours.append(contour)

# # for contour in filtered_contours:
# #     # Get bounding box coordinates
# #     x, y, w, h = cv.boundingRect(contour)
# #     cv.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)

# show(image)
# show(template)

# --------------------------------------------------------------------------


import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import os

MIN_MATCH_COUNT = 10

img1 = cv.imread('./assets/blank_single.jpg', cv.IMREAD_GRAYSCALE)  # queryImage
img2 = cv.imread('./assets/uno_red_3.jpg', cv.IMREAD_GRAYSCALE)  # trainImage

# Initiate SIFT detector
sift = cv.SIFT_create()

# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1, None)
kp2, des2 = sift.detectAndCompute(img2, None)

FLANN_INDEX_KDTREE = 1
index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
search_params = dict(checks=500)

flann = cv.FlannBasedMatcher(index_params, search_params)

matches = flann.knnMatch(des1, des2, k=2)

# store all the good matches as per Lowe's ratio test.
good = []
for m, n in matches:
    if m.distance < 0.8*n.distance:
        good.append(m)
        
if len(good) > MIN_MATCH_COUNT:
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

    M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC, 5.0)
    matchesMask = mask.ravel().tolist()

    h, w = img1.shape
    pts = np.float32([[0, 0], [0, h-1], [w-1, h-1], [w-1, 0]]
                     ).reshape(-1, 1, 2)
    dst = cv.perspectiveTransform(pts, M)

    img2 = cv.polylines(img2, [np.int32(dst)], True, 255, 3, cv.LINE_AA)

else:
    print("Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
    matchesMask = None

draw_params = dict(matchColor=(0, 255, 0),  # draw matches in green color
                   singlePointColor=None,
                   matchesMask=matchesMask,  # draw only inliers
                   flags=2)

img3 = cv.drawMatches(img1, kp1, img2, kp2, good, None, **draw_params)

plt.imshow(img3, 'gray'), plt.show()

# --------------------------------------------------------------------------------------
cv2 = cv


image, contours = img_preprocessing('./assets/uno_green_3.jpg')
template, _ = img_preprocessing('./assets/blank_single.jpg')

# Filter contours based on area or other criteria
filtered_contours = []
for contour in contours:
    area = cv2.contourArea(contour)
    if area > 500:  # Adjust this threshold as needed
        filtered_contours.append(contour)

for contour in filtered_contours:
    # Get bounding box coordinates
    x, y, w, h = cv2.boundingRect(contour)

    # Perform template matching in the region of interest
    roi = image[y:y+h, x:x+w]
    result = cv2.matchTemplate(roi, template, cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)

    print(max_val)
    # Template found, draw rectangle around the detected card
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

    # Optionally, you can also output the coordinates of the detected cards
    print("Uno card detected at coordinates:", (x, y))

image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
cv2.drawContours(image, filtered_contours, -1, (0, 0, 255), 5)
# # Display results
plt.imshow(image), plt.show()






# ---------------------------------------

def load_templates(directory):
    """ Load template images from a specified directory. """
    if not os.path.exists(directory):
        raise FileNotFoundError(f"Template directory '{directory}' not found.")
    templates = {}
    for filename in os.listdir(directory):
        if filename.endswith('.jpg'):
            number = filename.split('.')[0]  # assuming file name like "1.jpg"
            template = cv2.imread(os.path.join(directory, filename), 0)
            if template is not None:
                templates[number] = template
            else:
                print(f"Warning: Unable to load template for '{filename}'.")
    return templates

def preprocess_image(image_path):
    """ Load and preprocess an image. """
    if not os.path.exists(image_path):
        raise FileNotFoundError(f"No file found at {image_path}")
    image = cv2.imread(image_path)
    if image is None:
        raise FileNotFoundError(f"Failed to load the image at {image_path}")
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return image, gray

def detect_color(image):
    """ Detect the predominant color in an image. """
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    color_ranges = {
        'Red': ((0, 120, 70), (10, 255, 255)),
        'Green': ((100, 150, 0), (140, 255, 255)),
        'Blue': ((40, 40, 40), (80, 255, 255)),
        'Yellow': ((15, 150, 150), (35, 255, 255))
    }
    for color, (lower, upper) in color_ranges.items():
        mask = cv2.inRange(hsv, np.array(lower), np.array(upper))
        if mask.any():
            return color
    return 'Unknown'

def match_number(gray, templates):
    """ Match the number on the card using template matching. """
    best_match = ("Unknown", 0)
    for number, template in templates.items():
        res = cv2.matchTemplate(gray, template, cv2.TM_CCOEFF_NORMED)
        _, max_val, _, _ = cv2.minMaxLoc(res)
        if max_val > best_match[1]:
            best_match = (number, max_val)
    return best_match[0] if best_match[1] > 0.5 else "Unknown"

def main(image_path, template_dir):
    """ Main function to process an image and detect number and color. """
    templates = load_templates(template_dir)
    image, gray = preprocess_image(image_path)
    color = detect_color(image)
    number = match_number(gray, templates)
    
    print(f"Detected color: {color}")
    print(f"Detected number: {number}")

if __name__ == '__main__':
    template_dir = r"C:\Users\moune\Documents\University\Intelligent Sensing\W16-Image-Processing Files\Templets"
    image_path = r"C:\Users\moune\Documents\University\Intelligent Sensing\W16-Image-Processing Files\unocard.jpeg"
    main(image_path, template_dir)



# ---------------------------------
def get_target_image(img, debug=False) -> cv.typing.MatLike:
    # Resize image to workable size when stand alone
    # dim_limit = 1080
    # max_dim = max(img.shape)
    # if max_dim > dim_limit:
    #     resize_scale = dim_limit / max_dim
    #     img = cv.resize(img, None, fx=resize_scale, fy=resize_scale)
    # Create a copy of resized original image for later use
    orig_img = img.copy()

    # Repeated Closing operation to remove text from the document.
    kernel = np.ones((5, 5), np.uint8)
    img = cv.morphologyEx(img, cv.MORPH_CLOSE, kernel, iterations=5)
    show(img) if debug else None
    
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (9, 9), 0)
    show(blur) if debug else None
    
    _, thresh = cv.threshold(blur, 250, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
    show(thresh) if debug else None

    # Edge Detection.
    canny = cv.Canny(thresh, 0, 150)
    canny = cv.dilate(canny, cv.getStructuringElement(
        cv.MORPH_ELLIPSE, (5, 5)))
    show(canny) if debug else None

    # Finding contours for the detected edges.
    contours, hierarchy = cv.findContours(
        canny, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    # Keeping only the largest detected contour.
    page = sorted(contours, key=partial(
        cv.arcLength, closed=False), reverse=True)[:1]

    # Detecting Edges through Contour approximation.
    # Loop over the contours.
    if len(page) == 0:
        return orig_img
    x,y,w,h = cv.boundingRect(page[0])
    # epsilon = 0.02 * cv.arcLength(page[0], True)
    # corners = cv.approxPolyDP(page[0], epsilon, True)

    con = np.zeros_like(img)
    cv.drawContours(con, page[0], -1, (0, 255, 255), 3)
    # cv.drawContours(con, corners, -1, (0, 255, 0), 10)
    show(con) if debug else None

    # Sorting the corners and converting them to desired shape.
    # corners = sorted(np.concatenate(corners).tolist())
    # For 4 corner points being detected.
    # corners = order_points(corners)

    # destination_corners = find_dest(corners)

    h, w = orig_img.shape[:2]
    # Getting the homography.
    # M = cv.getPerspectiveTransform(np.float32(
    #     corners), np.float32(box))
    # # Perspective transform using homography.
    # final = cv.warpPerspective(orig_img, M, (destination_corners[2][0], destination_corners[2][1]),
    #                            flags=cv.INTER_LINEAR)

    return cv.resize(orig_img[x:x+w,y:y+h], (264, 264))