try:
    import os
    # Suppress TensorFlow log messages
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
except:
    pass
from datetime import datetime
from keras.api.applications.vgg16 import VGG16
from matplotlib import pyplot as plt
import tensorflow as tf
from pathlib import Path
from keras.api.callbacks import ModelCheckpoint, EarlyStopping
from keras.api import layers
from keras.api import Sequential
from icecream import ic
import numpy as np
import utils
import target_detection_hough

batch_size = 32
img_height = 150
img_width = 150
AUTOTUNE = tf.data.AUTOTUNE


data_dir = Path(__file__).parent.parent.joinpath('./data/')

list_ds = tf.data.Dataset.list_files(str(data_dir/'*/*'), shuffle=True)
image_count = len(list(data_dir.glob('*/*.png')))
# list_ds = list_ds.shuffle(image_count, reshuffle_each_iteration=False)
ic(image_count)

class_names = np.array(sorted([item.name for item in data_dir.glob('*')]))
ic(class_names)

for file_name in list_ds.take(1):
    ic(file_name.numpy())

# list_ds = list_ds.take(33)


def stuff_on_image(img):
    # @tf.py_function(Tout=(tf.uint16,tf.uint16))
    number_image, image, status = target_detection_hough.detect_target(
        img.numpy())
    _pre_processed_number_image = utils.img_edges(number_image)
    if status:
        return _pre_processed_number_image
    return None


def get_label(file_path):
    # Convert the path to a list of path components
    parts = tf.strings.split(file_path, os.path.sep)
    # The second to last is the class-directory
    one_hot = parts[-2] == class_names
    # Integer encode the label
    return tf.argmax(one_hot)


def decode_img(img):
    # Convert the compressed string to a 3D uint8 tensor
    img = tf.io.decode_image(img, channels=3, expand_animations=False)
    # Resize the image to the desired size
    image = tf.py_function(stuff_on_image, [img], Tout=tf.uint16)
    return image
    return img


def process_path(file_path):
    label = get_label(file_path)
    # Load the raw data from the file as a string
    img = tf.io.read_file(file_path)
    img = decode_img(img)
    label.set_shape([])
    img.set_shape((150,150,1))
    return img, label


def configure_for_performance(ds: tf.data.Dataset):
    ds = ds.cache()
    ds = ds.shuffle(buffer_size=1000)
    ds = ds.batch(batch_size)
    ds = ds.prefetch(buffer_size=AUTOTUNE)
    return ds


list_ds = list_ds.map(process_path, num_parallel_calls=AUTOTUNE).filter(
    lambda x, y: x is not None)

for image, label in list_ds.take(1):
    input_shape = image.numpy().shape
    ic("Image shape: ", input_shape)
    ic("Label: ", label.numpy())

list_ds = configure_for_performance(list_ds)

val_size = int(image_count * 0.2)
train_ds = list_ds.skip(val_size)
val_ds = list_ds.take(val_size)

ic(train_ds)

# image_batch, label_batch = next(iter(list_ds))

# plt.figure(figsize=(10, 10))
# for i in range(batch_size):
# ax = plt.subplot(3, 3, i + 1)
# plt.imshow(image_batch[i].numpy().astype("uint8"))
# label = label_batch[i]
# plt.title(class_names[label])
# plt.axis("off")
# utils.show(image_batch[i].numpy().astype(
#     "uint8"), cv_show=True, cvt=True, auto=500)
# plt.show()

if __name__ == '__main__':
    epoch = 5
    model = Sequential([
        layers.Rescaling(1./255, input_shape=(150, 150, 1)),
        layers.RandomRotation(0.1),
        layers.Conv2D(32, 3, activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dense(len(class_names))
    ])
    # vgg16 = VGG16(weights='imagenet',
    #               input_shape=input_shape,
    #               classes=len(class_names),
    #               include_top=False)
    # for layer in vgg16.layers:
    #     layer.trainable = False
    model.compile(
        optimizer='adam',
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=['accuracy'])
    early_stopping = EarlyStopping(
        monitor='val_loss', patience=3, restore_best_weights=True)
    checkpoint = ModelCheckpoint(
        f"best_model_{datetime.now().strftime('%d%m%y%H%M%S')}.keras", save_best_only=True, monitor='val_accuracy', mode='max')
    result = model.fit(train_ds,
                       validation_data=val_ds,
                       epochs=epoch,
                       callbacks=[early_stopping, checkpoint],
                       batch_size=batch_size
                       )
