import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
import shutil 

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.models import load_model
from sklearn.metrics import confusion_matrix, classification_report 
vc = cv2.VideoCapture(0) 

print(vc.isOpened())
i = 0
while vc.isOpened():
    rval, frame = vc.read()    # read video frames again at each loop, as long as the stream is open
    cv2.imwrite(f'D:/UNO_CARD/YELLOW_DRAW_2/{i}.png',frame)
    i+=1
    cv2.imshow("stream", frame)# display each frame as an image, "stream" is the name of the window
    key = cv2.waitKey(1)       # allows user intervention without stopping the stream (pause in ms)
    if key == 27:              # exit on ESC
        break
cv2.destroyWindow("stream")    # close image window upon exit
vc.release() 

source_dirs = ['D:/UNO_CARD/BLUE_9', 'D:/UNO_CARD/GREEN_9', 'D:/UNO_CARD/RED_9','D:/UNO_CARD/YELLOW_9']
target_dir = 'D:/UNO_CARD/9' 
if not os.path.exists(target_dir):
    os.makedirs(target_dir)
file_counter = 0 
for directory in source_dirs:
    # List all files in the directory and sort them to maintain order
    files = sorted(os.listdir(directory))
    # Rename and move each file
    for filename in files:
        # Set up the new filename based on the counter
        _, file_extension = os.path.splitext(filename)
        new_filename = f"{file_counter}{file_extension}"
        # Build the full source and target paths
        source_file = os.path.join(directory, filename)
        target_file = os.path.join(target_dir, new_filename)
        # Move and rename the file
        shutil.move(source_file, target_file)
        # Increment the counter for the next file
        file_counter += 1
print(f"Files have been successfully renamed and moved. Last file number: {file_counter-1}") 

def load_images_and_labels(base_path):
    images = []
    labels = []
    for folder in os.listdir(base_path):
        folder_path = os.path.join(base_path, folder)
        for file in os.listdir(folder_path):
            img_path = os.path.join(folder_path, file)
            img = cv2.imread(img_path)
            img = cv2.resize(img, (64, 64))  # Resize to normalize image size
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert to grayscale
            images.append(img)
            labels.append(int(folder))  # Assuming folder names are 0-9
    images = np.array(images) / 255.0  # Normalize pixel values
    images = images.reshape(-1, 64, 64, 1)  # Reshape for CNN input
    labels = to_categorical(labels, num_classes=10)  # One-hot encode labels
    return images, labels

# Path to your dataset
base_path = 'D:/UNO_CARD'
images, labels = load_images_and_labels(base_path)
print(len(images),len(labels)) 

model = Sequential([
    Conv2D(32, (3, 3), activation='relu', input_shape=(64, 64, 1)),
    MaxPooling2D(2, 2),
    Conv2D(64, (3, 3), activation='relu'),
    MaxPooling2D(2, 2),
    Flatten(),
    Dense(128, activation='relu'),
    Dropout(0.5),
    Dense(10, activation='softmax')
])

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy']) 
checkpoint = ModelCheckpoint('best_model.h5', save_best_only=True, monitor='val_accuracy', mode='max')
history = model.fit(images, labels, epochs=20, validation_split=0.2, callbacks=[checkpoint]) 
test_base_path = 'D:/UNO_CARD'  # Update this path to where your test data is located
test_images, test_labels = load_images_and_labels(test_base_path) # Load the model
model = load_model('best_model.keras') 
loss, accuracy = model.evaluate(test_images, test_labels)
print(f"Test loss: {loss}")
print(f"Test accuracy: {accuracy}") 
predictions = model.predict(test_images)
predicted_classes = np.argmax(predictions, axis=1)
actual_classes = np.argmax(test_labels, axis=1)

# Display some predictions
for i in range(10):  # Show first 10 predictions
    print(f"Predicted: {predicted_classes[i]}, Actual: {actual_classes[i]}") 

cm = confusion_matrix(actual_classes, predicted_classes)
print(cm)

report = classification_report(actual_classes, predicted_classes)
print(report) 

loss, accuracy = model.evaluate(test_images, test_labels)
print(f"Test Loss: {loss}")
print(f"Test Accuracy: {accuracy}") # Predict the test set results
predictions = model.predict(test_images)
predicted_classes = np.argmax(predictions, axis=1)
actual_classes = np.argmax(test_labels, axis=1) # Generate the confusion matrix
cm = confusion_matrix(actual_classes, predicted_classes)
print("Confusion Matrix:")
print(cm) # Generate a classification report
report = classification_report(actual_classes, predicted_classes)
print("Classification Report:")
print(report) 
def plot_images(images, actuals, preds):
    plt.figure(figsize=(10, 10))
    for i in range(25):  # adjust this number based on how many images you want to show
        ax = plt.subplot(5, 5, i + 1)  # adjust the grid size accordingly
        plt.imshow(images[i].reshape(64, 64), cmap='gray')  # adjust the shape based on your actual image dimensions
        plt.title(f"Actual: {actuals[i]}, Pred: {preds[i]}")
        plt.axis('off')
    plt.show()

# Call the function with the appropriate arrays
plot_images(test_images, actual_classes, predicted_classes) 
model = load_model('best_model.h5') 

def predict_image(model, image_path):
    # Preprocess the image
    processed_image = preprocess_image(image_path)
    # Predict the class
    prediction = model.predict(processed_image)
    predicted_class = np.argmax(prediction)
    return predicted_class 
image_path = 'D:/UNO_CARD/puppy.png'
predicted_class = predict_image(model, image_path)
print(f"Predicted class: {predicted_class}") 

def display_image_with_prediction(image_path, predicted_class):
    img = cv2.imread(image_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert to RGB for matplotlib
    plt.imshow(img)
    plt.title(f"Predicted class: {predicted_class}")
    plt.axis('off')
    plt.show()

# Display the image with its prediction
display_image_with_prediction(image_path, predicted_class) 

def preprocess_image(image_path):
    # Load the image
    img = cv2.imread(image_path)
    # Resize the image to match the input size of the model (e.g., 64x64)
    img = cv2.resize(img, (64, 64))
    # Convert to grayscale if the model was trained on grayscale images
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Normalize pixel values to [0, 1]
    img = img / 255.0
    # Reshape the image to fit the model input (e.g., (1, 64, 64, 1) for a single grayscale image)
    img = img.reshape(1, 64, 64, 1)
    return img