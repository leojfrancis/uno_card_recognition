try:
    import os
    import ntpath as nt
    # Suppress TensorFlow log messages
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
except:
    pass
import argparse
from time import sleep
from typing import List
import cv2 as cv
import sys
import validators
import logging
from src import target_recognition
from src import utils
from src import target_detection_hough

HELP_STRING = '\nuse -h for help or refer ./README.md'


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# formatter = logging.Formatter('%(asctime)s:%(name)s:%(message)s')

stream_handler = logging.StreamHandler()
# stream_handler.setFormatter(formatter)

logger.addHandler(stream_handler)



def parse_args(argv: List[str]):
    """
    Method to parse arguments
    """
    parser = argparse.ArgumentParser(description="Uno Card Detection and Recognition CLI",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "src", nargs='?', help="Image Source or video capture link.", default='0')
    parser.add_argument("-i", "--image", action="store_true",
                        help="Processes still image at the src location if False video capture starts until exit(0)", default=False)
    parser.add_argument("-s", "--show", action="store_true", help="Show Image")
    parser.add_argument("-a", "--auto", action="store_true",
                        help="Automatic Image slideshow works only with --show argument", default=False)
    parser.add_argument("-t", "--test-video", action="store_true", help="Test video script")
    

    args = parser.parse_args(argv[1:])
    return vars(args)


def process(img, model=None):
    """
    Process the input image to detect and recognize a target.
    Returns a tuple containing the detected value, status, and the region of interest (ROI) image.
    """
    _image = utils.preprocess_image(img)
    _number_roi, _image_roi, status = target_detection_hough.detect_target(
        _image)
    if not status:
        return None, False, None
    _number_edge = utils.img_edges(_number_roi)
    recognised_value, status = target_recognition.predict(_number_edge, model)
    if not status:
        return None, False, None
    _color_code, _ = utils.find_color(_image_roi)
    color_string = utils.color_string(_color_code)
    if recognised_value not in ['wild', 'draw4']:
        val = f"{color_string} - {recognised_value}"
    else:
        val = f"{recognised_value} card"
    return val, True, _number_edge


def video_stream(src=0, retry=5, show=True, test = False):
    """
    Start a video stream and process each frame to detect and recognize targets.
    """
    init = False
    # vc = cv.VideoCapture('http://localhost:4747/video')
    # vc = cv.VideoCapture('http://192.168.1.3:4747/video')
    vc = cv.VideoCapture(src)
    while (retry != 0 and not vc.isOpened()):
        logger.warning("Connection to feed Failed")
        retry -= 1
        if retry == 0:
            exit(0)
    while vc.isOpened():
        if not init:
            logger.info("Video input processing started..")
            init = True
        # read video frames again at each loop, as long as the stream is open
        rval, frame = vc.read()
        val, status, img = process(frame)
        # display each frame as an image, "stream" is the name of the window
        if status:
            logger.info(val)
        if show:
            key = utils.show(img, "show", cv_show=True, auto=True)
            # allows user intervention without stopping the stream (pause in ms)
            if key == 27:              # exit on ESC
                break
        if test:
            break
    # if show:
        # cv.destroyAllWindows("stream")    # close image window upon exit
    vc.release()

def main(argv: List[str]):
    """
    Main function to handle command-line arguments and initiate the appropriate mode (image or video).
    """
    config = parse_args(argv)
    src = config['src']
    _show = config['show']
    _auto = config['auto']
    _test = config['test_video']
    _image_mode = config['image']
    if _image_mode:
        if os.path.isfile(src):
            logger.info(f"Processing file {src} ...")
            value, status, img = process(utils.get_image(src))
            if status:
                logger.info(
                    f"{value} for image in location {src}")
                if _show:
                    utils.show(img, cv_show=True, auto=_auto)
                    sleep(0.3)
            else:
                logger.warning("Unable To process Image try with other image this may be due to unsupported file type, camera placement or lighting"+HELP_STRING)
        elif os.path.isdir(src):
            logger.info(f"Processing images in directory {src} ...")
            out = utils.process_in_directory(src, process)
            for output in out:
                value, status, img, path = output
                if status:
                    logger.info(
                        f"{value} for image in location {path}")
                    if _show:
                        utils.show(img, cv_show=True, auto=_auto)
                        sleep(0.3)
                else:
                    logger.warning(f"Unable To process Image {path}")
        else:
            logger.warning("Not a folder or file"+HELP_STRING)
    else:
        if src.isdigit():
            video_stream(int(src), show=_show, test=_test)
        elif os.path.exists(src):
            logger.warning(
                f"Looks like {src} is a valid path please add appropriate tag (-i) or"+HELP_STRING)
        elif validators.url(src):
            video_stream(src, show=_show, test=_test)
        else:
            logger.warning("Not a valid link"+HELP_STRING)
    return 0


if __name__ == '__main__':
    try:
        sys.exit(main(sys.argv))
    except KeyboardInterrupt:
        logger.info('\nProgram Exited successfully')
        try:
            sys.exit(130)
        except SystemExit:
            os._exit(130)
