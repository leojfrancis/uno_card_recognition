run:
	python3 src/main.py data/0/0.png -i
	
detection:
	python3 ./src/target_detection_hough.py

zip:
	zip -r submissions/$(shell date +"%F_%T").zip . -x "**/__pycache__/*" "**/capture/*" "**/data_old/*" "sample/*" ".*" "submissions/*" "venv/*" Makefile

clean:
	py3clean .