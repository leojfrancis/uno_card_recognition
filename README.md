# Uno Detector using Image Processing opencv

Uno Detector is a Python-based application that detects and recognizes Uno cards in real-time using a webcam feed or from static images. The application leverages image processing techniques and machine learning models to accurately identify and classify Uno cards.

## Table of Contents

- [Uno Detector using Image Processing opencv](#uno-detector-using-image-processing-opencv)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
  - [Installation](#installation)
  - [Usage](#usage)
    - [Real-time Video Stream](#real-time-video-stream)
    - [Static Image Processing](#static-image-processing)
    - [Command-line Arguments](#command-line-arguments)
  - [Configuration](#configuration)
  - [File Structure](#file-structure)
  - [Important Dependencies](#important-dependencies)
  - [Functionality Overview](#functionality-overview)
    - [1. Image Acquisition](#1-image-acquisition)
    - [2. Preprocessing](#2-preprocessing)
    - [3. Region of Interest (ROI) Extraction](#3-region-of-interest-roi-extraction)
    - [4. Card Outline Detection](#4-card-outline-detection)
    - [5. Card Value and Color Identification](#5-card-value-and-color-identification)
  - [Contributing](#contributing)
    - [Future Features](#future-features)
  - [License](#license)

## Features

- **Real-time video stream processing**: Detect and recognize Uno cards from a live webcam feed.
- **Static image processing**: Detect and recognize Uno cards from static images in a specified directory.
- **Card recognition**: Predict the value and color of the detected Uno card using a pre-trained model.
- **Image display**: Optionally displays the processed images with the detected cards.

## Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/leojfrancis/uno_card_recognition.git
    cd uno_card_recognition
    ```

2. Install the required dependencies:
    ```bash
    pip install -r requirements.txt
    ```

3. Download the pre-trained model and place it in the `src` directory.

## Usage
- The outputs for the script are print statements in the command line interface.
- If your machine supports docker then spinup the container to show sample help screen (optional):
```bash
docker-compose up
```
### Real-time Video Stream

To start the Uno card detection from a live video stream, run:
```bash
python main.py <link-to-camera-feed (optional)>
```

### Static Image Processing

To process a single image:
```bash
python main.py -i <path/to/image.png>
```

To process all images in a directory while displaying the sideshow of the ROI of images:
```bash
python main.py -i -s -a <path/to/image_directory>
```

### Command-line Arguments

-  `-h`, `--help`:   show this help message and exit
-  `-i`, `--image`: Processes still image at the src location if false video capture starts until exit(0) (default: False)
-  `-s`, `--show`:   Show Image (default: False)
-  `-a`, `--auto`:  Automatic Image slideshow works only with `--show` argument (default: False)

## Configuration

The script uses command-line arguments to adjust its settings. By default, it initiates a real-time video stream and processes the feed until the exit key (Q) is pressed or the script is stopped.

The program is intended for use with a camera positioned in a specific way. The camera should be aimed at a clean table with a dark background from a distance of 10cm-20cm. Cards can then be placed within the camera's field of view for detection.

## File Structure

- `main.py`: Entry point for the application.
- `src/data_collection.py`: Helper functions to capture and save uno card images for generation of datasets.
- `src/utils.py`: Utility functions for image processing and card recognition.
- `src/target_recognition.py`: Functions using ML for recognizing card values.
- `src/target_detection_hough.py`: Functions and classes for detecting cards using the Hough transform.
- `src/models/new_model.keras`: Pre-trained model for card recognition (you need to place your model here).
- `src/train_with_generator.ipynb`: Utility to train model here. This can be used to train custom models with custom parameters and data too. Sample data is uploaded in the [Storage Link](https://mega.nz/folder/s5UggIyR).

## Important Dependencies

There can be some machine Prerequisites to install these libraries. Sample setup can be found in the [Dockerfile](./Dockerfile)
- OpenCV
- TensorFlow/Keras
- NumPy
- Scikit-learn
- Matplotlib
- Tqdm

Install all dependencies using:
```bash
pip install -r requirements.txt
```

## Functionality Overview

By following these steps, the script effectively identifies and isolates Uno cards, determines their values, and detects their colors.

### 1. Image Acquisition
- **Source**: Image data is obtained from the camera feed or a folder.

### 2. Preprocessing
- **Resize**: The data is resized to a predefined size for consistent processing.

### 3. Region of Interest (ROI) Extraction
- **Denoising**: Noise is removed from the image.
- **Grayscale Conversion**: The image is converted to grayscale.
- **Thresholding**: Thresholding is applied to focus on intensities above a predetermined threshold.
- **Morphological Operations**: Close and open operations are performed to eliminate noise within and outside the target area.
- **Edge Detection**: Canny edge detection is used to identify edges.
- **Edge Enhancement**: Dilation is applied to increase the prominence of the edges, leaving only the card outline.

### 4. Card Outline Detection
- **Line Detection**: Lines are detected using the Hough Transform.
- **Corner Extraction**: Detected lines are used to identify the card corners.
- **Perspective Transform**: The corners are used to apply a perspective transform, isolating just the card section of the image.

### 5. Card Value and Color Identification
- **Value Detection**:
  - **Cropping**: The middle section of the card, which contains the value, is cropped out.
  - **Model Prediction**: The cropped section is sent to a pre-trained model to identify the card value.
- **Color Detection**:
  - **Flattening**: The ROI is flattened.
  - **K-means Clustering**: The image is converted from a full-color spectrum to three primary colors.
  - **Dominant Color Identification**: The color with the highest count is determined as the card's dominant color.

## Contributing

Contributions are welcome! Please open an issue or submit a pull request.
Run the following command to run tests 
```bash
python3 -m unittest ./tests/main_test.py
```
### Future Features

- `threaded_camera.py`: Classes for handling camera input in a separate thread. This can be integrated.
- `using custom cv2 haarcascades for recognition`: Use this to increase speed.

## License

This project is licensed under the MIT License.