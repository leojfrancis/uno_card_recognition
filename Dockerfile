# syntax=docker/dockerfile:1

FROM tensorflow/tensorflow
WORKDIR /code
# RUN apk add --no-cache gcc musl-dev linux-headers
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
COPY ./src ./src
COPY ./main.py ./main.py
COPY ./tests ./tests
COPY ./data/0/0.png ./0.png
# CMD ["python3", "-m", "unittest",  "./tests/main_test.py"]