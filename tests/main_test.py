import unittest
import warnings
import main as application
import logging

class ScriptTests(unittest.TestCase):
    def setUp(self):
        warnings.simplefilter('ignore', category=Warning)
        logging.disable(logging.CRITICAL)

    def test_image(self):
        _ = application.main(['main.py', '-i', './data/0/0.png'])
        self.assertEqual(_, 0)

    def test_image_fail(self):
        _ = application.main(['main.py', '-i', './data/0/0.jpeg'])
        self.assertEqual(_, 0)

    def test_folder(self):
        _ = application.main(['main.py', '-i','./data_old/capture'])
        self.assertEqual(_, 0)

    def test_folder_fail(self):
        _ = application.main(['main.py', '-i','./data/'])
        self.assertEqual(_, 0)

    def test_video(self):
        _ = application.main(['main.py', '-t'])
        self.assertEqual(_, 0)

    def test_video_fail(self):
        _ = application.main(['main.py', '-t', './data'])
        self.assertEqual(_, 0)