import cv2 as cv
import os
from time import sleep
import numpy as np


def check_key_strokes():
    """
    method to identify key stroke codes
    """
    while True:
        cv.imshow("stream", np.zeros((100, 100)))
        key = cv.waitKey(1)
        if key == 27:              # exit on ESC
            break
        elif key != -1:
            print(f"{key = }") if key != -1 else None
    cv.destroyWindow("stream")    # close image window upon exit


def collect_data(src=0, data_directory='../data'):
    """
    collect card images and save them in corresponding folders based on keystrokes
    """
    if src == 0:
        vc = cv.VideoCapture('http://192.168.1.4:4747/video')
    if src == 1:
        vc = cv.VideoCapture('http://localhost:4747/video')
    if src == 2:
        vc = cv.VideoCapture(0)

    img_index = 0
    img_type = -1
    _next = 0
    if not vc.isOpened():
        print(vc.isOpened())
        return
    while vc.isOpened():
        # read video frames again at each loop, as long as the stream is open
        rval, frame = vc.read()
        # display each frame as an image, "stream" is the name of the window
        cv.imshow("stream", frame)
        # allows user intervention without stopping the stream (pause in ms)
        key = cv.waitKey(1)
        if key == 27:              # exit on ESC
            break
        elif key == 110:
            _next = 1
        elif key != -1:
            # print(f"{key = }") if key != -1 else None
            if key < 100:
                img_type = key - 48
            if key == 113:
                img_type = 'skip'
            elif key == 119:
                img_type = 'reverse'
            elif key == 101:
                img_type == 'draw2'
            elif key == 114:
                img_type == 'draw4'
            elif key == 116:
                img_type == 'wild'
            print(f"{img_type = }")
            sleep(2)
        elif img_index == 100:
            img_type = -1
            img_index = 0
            _next = 0
        elif img_type != -1:
            directory = f'{data_directory}/{img_type}'
            if _next:
                pass
            elif img_index >75:
                _next = 0
                print('.', end='')
                continue
            if not os.path.exists(directory):
                os.makedirs(directory)
            cv.imwrite(f"{directory}/{img_index}.png", frame)
            img_index += 1
            print(f"{img_index = } {img_type = }")
            # sleep(0.3)

    cv.destroyWindow("stream")    # close image window upon exit
    vc.release()


def save_image_from_server(directory=f'./capture/'):
    """
    Save a single image from source
    """
    # vc = cv.VideoCapture('http://192.168.1.4:4747/video')
    # vc = cv.VideoCapture('http://localhost:4747/video')
    vc = cv.VideoCapture(0)
    print(vc.isOpened())
    while vc.isOpened():
        # read video frames again at each loop, as long as the stream is open
        rval, frame = vc.read()
        # display each frame as an image, "stream" is the name of the window
        cv.imshow("stream", frame)
        # allows user intervention without stopping the stream (pause in ms)
        key = cv.waitKey(1)
        if key == 27:              # exit on ESC
            break
        elif key != -1:
            # print(f"{key = }") if key != -1 else None
            key = key - 48
            if not os.path.exists(directory):
                os.makedirs(directory)
            cv.imwrite(f"{directory}/{key}.png", frame)
            key = -1
            print(f"{directory}/{key}.png")

    cv.destroyWindow("stream")    # close image window upon exit
    vc.release()
if __name__ == '__main__':
    # check_key_strokes()
    save_image_from_server()
