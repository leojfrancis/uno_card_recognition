from keras.api.models import save_model, load_model
import tensorflow as tf
import numpy as np
from pathlib import Path

# loaded_model = load_model(Path(__file__).parent.joinpath('./models/best_model_120724011444.keras'))
loaded_model = load_model(Path(__file__).parent.joinpath('./models/new_model.keras'))
classes = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'draw2', 'draw4',
           'reverse', 'skip', 'wild']


def predict(_image, model=loaded_model):
    """
    predict the image using the saved model
    """
    if not model:
        model = loaded_model
    number_image = _image / 255
    predictions = model.predict(np.array([number_image]), verbose=0)
    score = tf.nn.softmax(predictions)
    return classes[np.argmax(score)], True
