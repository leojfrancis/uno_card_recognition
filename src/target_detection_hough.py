from typing import Tuple
import cv2 as cv
import numpy as np
import math
from itertools import combinations
from sklearn.cluster import KMeans
from src import utils
from .utils import show


def order_points(pts):
    """
    Rearrange coordinates to order: top-left, top-right, bottom-right, bottom-left.
    """
    rect = np.zeros((4, 2), dtype='float32')
    pts = np.array(pts)
    s = pts.sum(axis=1)
    # Top-left point will have the smallest sum.
    rect[0] = pts[np.argmin(s)]
    # Bottom-right point will have the largest sum.
    rect[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis=1)
    # Top-right point will have the smallest difference.
    rect[1] = pts[np.argmin(diff)]
    # Bottom-left will have the largest difference.
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return rect


def find_dest(pts):
    """
    Find the destination points for perspective transformation.
    """
    (tl, tr, br, bl) = pts
    # Finding the maximum width.
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # Finding the maximum height.
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # Final destination co-ordinates.
    destination_corners = np.array([[0, 0], [maxWidth-1, 0],
                                    [maxWidth-1, maxHeight-1], [0, maxHeight-1]], dtype='float32')

    return destination_corners


def draw_lines(img, lines):
    """
    Draw lines on the image.
    """
    image = img.copy()
    if len(lines) > 0:
        for i in range(0, len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a, b = np.cos(theta), np.sin(theta)
            x0, y0 = a * rho, b * rho
            n = 5000
            x1 = int(x0 + n * (-b))
            y1 = int(y0 + n * (a))
            x2 = int(x0 - n * (-b))
            y2 = int(y0 - n * (a))
            cv.line(
                image,
                (x1, y1),
                (x2, y2),
                (255, 0, 0),
                2
            )
    return image


def get_angle_between_lines(line_1, line_2):
    """
    Calculate the angle between two lines.
    """
    rho1, theta1 = line_1
    rho2, theta2 = line_2
    # x * cos(theta) + y * sin(theta) = rho
    # y * sin(theta) = x * (- cos(theta)) + rho
    # y = x * (-cos(theta) / sin(theta)) + rho
    m1 = -(np.cos(theta1) / np.sin(theta1))
    m2 = -(np.cos(theta2) / np.sin(theta2))
    return abs(math.atan(abs(m2-m1) / (1 + m2 * m1))) * (180 / np.pi)


def intersection(line1, line2):
    """
    Finds the intersection of two lines given in Hesse normal form.

    Returns closest integer pixel locations.
    """
    rho1, theta1 = line1
    rho2, theta2 = line2

    A = np.array([
        [np.cos(theta1), np.sin(theta1)],
        [np.cos(theta2), np.sin(theta2)]
    ])

    b = np.array([[rho1], [rho2]])
    x0, y0 = np.linalg.solve(A, b)
    x0, y0 = int(np.round(x0)), int(np.round(y0))
    return [[x0, y0]]


def get_intersections(lines, _image):
    """
    Finds the intersections between groups of lines.
    """
    intersections = []
    group_lines = combinations(range(len(lines)), 2)
    def x_in_range(x): return 0 <= x <= _image.shape[1]
    def y_in_range(y): return 0 <= y <= _image.shape[0]

    for i, j in group_lines:
        line_i, line_j = lines[i][0], lines[j][0]

        if 80.0 < get_angle_between_lines(line_i, line_j) < 100.0:
            int_point = intersection(line_i, line_j)

            if x_in_range(int_point[0][0]) and y_in_range(int_point[0][1]):
                intersections.append(int_point)
    return intersections


def find_lines(marked_image):
    """
    Find lines in the marked image using Hough transform.
    """
    lines = cv.HoughLines(marked_image, 2, np.pi / 360, 150)
    if lines is None:
        return marked_image, False
    if lines.size == 0:
        return marked_image, False
    return lines, True


def find_quadrilaterals(marked_image, lines):
    """
    Find quadrilaterals (corners) from the detected lines.
    """
    _intersections = get_intersections(lines, marked_image)
    if _intersections:
        X = np.array([[point[0][0], point[0][1]] for point in _intersections])
        if not len(X) > 4:
            return None, None, False
        kmeans = KMeans(
            n_clusters=4,
            init='k-means++',
            max_iter=100,
            n_init=10,
            random_state=0
        ).fit(X)
        return [[center.tolist()] for center in kmeans.cluster_centers_], kmeans, True
    return None, None, False


def draw_quadrilaterals(image, lines, kmeans):
    """
    Draw quadilaterals on image based on lines and intersections
    """
    grouped_output = image.copy()
    for idx, line in enumerate(lines):
        rho, theta = line[0]
        a, b = np.cos(theta), np.sin(theta)
        x0, y0 = a * rho, b * rho
        n = 5000
        x1 = int(x0 + n * (-b))
        y1 = int(y0 + n * (a))
        x2 = int(x0 - n * (-b))
        y2 = int(y0 - n * (a))
        cv.line(
            grouped_output,
            (x1, y1),
            (x2, y2),
            (0, 0, 255),
            2
        )
    for point in kmeans.cluster_centers_:
        x, y = point

        cv.circle(
            grouped_output,
            (int(x), int(y)),
            5,
            (255, 255, 255),
            5
        )
    return grouped_output


def get_roi_marked(_image, debug=False):
    """
    Get the region of intrest marked in the image
    """
    _de_noised = cv.fastNlMeansDenoising(_image, h=7)
    _grey = cv.cvtColor(_de_noised, cv.COLOR_BGR2GRAY)
    _, thresh = cv.threshold(
        _grey, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
    _kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (9, 9))
    opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, _kernel)
    image_morph_close = cv.morphologyEx(
        opening, cv.MORPH_CLOSE, _kernel, iterations=10)
    _canny = cv.Canny(image_morph_close, 50, 150, apertureSize=3)
    _canny_dilated = cv.dilate(_canny, np.ones((3, 3), dtype='uint8'))
    if debug:
        show(_de_noised)
        show(_grey)
        show(thresh)
        show(opening)
        show(image_morph_close)
        show(_canny)
        show(_canny_dilated)
    return _canny_dilated, _de_noised


def get_roi(image, debug=False) -> Tuple[cv.typing.MatLike, bool]:
    """
    extract the region of intrest from the image by performing hugh transform
    """
    marked_image, _image = get_roi_marked(image, debug)
    if debug:
        show(marked_image)
    lines, status = find_lines(marked_image)
    if debug:
        show(draw_lines(marked_image, lines))
    if status:
        corners, kmeans, status = find_quadrilaterals(marked_image, lines)
        if not status:
            return _image, False
        if debug:
            show(draw_quadrilaterals(_image, lines, kmeans))
        _corners = np.concatenate(corners)
        ordered_pts = order_points(_corners)
        # pcenter = utils.get_mid_point(ordered_pts[0], ordered_pts[2])
        for _point in ordered_pts:
            for _compare_point in ordered_pts:
                if (_point == _compare_point).all():
                    continue
                if utils.inside_circle(_point, _compare_point, 25):
                    return _image, False
        destination_corners = find_dest(ordered_pts)
        # Getting the homography.
        M = cv.getPerspectiveTransform(
            ordered_pts, destination_corners)
        # Perspective transform using homography.
        final = cv.warpPerspective(_image, M, np.array((destination_corners[2][0] + 1, destination_corners[2][1]+1)).astype(int),
                                   flags=cv.INTER_LINEAR)
        # show(final)
        final = cv.resize(final, (300, 300))
        return final, True
    return _image, False


def detect_target(_image):
    """
    Detect the target in the image using Hough transform and KMeans clustering.
    """
    image, status = get_roi(_image, debug=False)
    if status:
        return utils.get_value_roi(image), image, True
    return None, None, False


if __name__ == "__main__":
    # show(detect_target('./data/wild/0.png'))
    # for i in range(1,2):
    for j in range(10):
        _image = utils.get_and_preprocess_image(f'./data/{j}/0.png')
        image, status = get_roi(_image, debug=False)
        if status:
            number_image = utils.get_value_roi(image)
            number_image = utils.img_edges(number_image)
            show(number_image, 'video', cv_show=1, auto=0)
        print(f"{''},{j},{status}")
        print(number_image.size)
    cv.destroyAllWindows()
    # (145, 135) size of image
