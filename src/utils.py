import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import os
import warnings
import math
from datetime import datetime
import time

from tqdm import tqdm
warnings.filterwarnings('ignore')


def listToNp(data):
    return np.array([np.array(val) for val in data])


def get_mid_point(point1, point2):
    return (point1[0] + point2[0]) // 2, (point1[1] + point2[1]) // 2


def inside_circle(point, pcenter, radius):
    x, y = point
    h, k = pcenter
    return (x - h) ** 2 + (y - k) ** 2 - radius ** 2 < 0


def euclidian_distance(a, b):
    math.sqrt((a[0] - b[0])**2 - (a[1] - b[1])**2)


def img_edges(image: str, blur=(5, 5)):
    # Apply GaussianBlur to reduce noise
    _blurred = cv.GaussianBlur(image, blur, 2)
    # Apply Canny edge detection
    _canny = cv.Canny(_blurred, 50, 150)
    _canny_dilated = cv.dilate(_canny, np.ones((3, 3), dtype='uint8'))
    # contours, hierarchy = cv.findContours(
    #     _canny, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)
    # hierarchy = hierarchy[0]
    # _base_image = np.zeros(image.shape)
    # for i, contour in enumerate(contours):
    #     if hierarchy[i][2] > 0 and hierarchy[i][3] > 0:
    #         cv.drawContours(_base_image, contours, i, (255, 0, 0), 2)
    return _canny_dilated.reshape(150, 150, 1)


def color_string(code):
    colors = ['blue', 'green', 'red', 'yellow']
    b, g, r = code
    if b < 100 and r > 150 and g > 150:
        return colors[-1]
    return colors[np.argmax(code)]


def show(image, name='', cvt=True, cv_show=False, auto=False, save=False):
    if image is None:
        return
    height, width = image.shape[0], image.shape[1]
    if not width > 0 and not height > 0:
        return
    if save:
        img = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        cv.imwrite(f"{int(time.time())}.jpg", img)
    if not cv_show:
        if cvt:
            image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        plt.imshow(image), plt.show()
    else:
        cv.imshow(f"{name}", image)
        return cv.waitKey(int(auto))


def find_color(image):
    # Number of clusters (k)
    k = 3
    # Reshape image into a 2D array of pixels
    pixels = image.reshape((-1, 3))
    # Convert to float type
    # Define stopping criteria for K-means algorithm
    final_pixels = []
    for pixel in pixels:
        if pixel[0] > 100 and pixel[1] > 100 and pixel[2] > 100:
            continue
        final_pixels.append(pixel)
    if not len(final_pixels) >= k:
        return (0, 0, 0), np.zeros((5, 5, 3), np.uint8)
    pixels = np.float32(final_pixels)
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 100, 0.3)

    # Perform K-means clustering
    _, labels, centers = cv.kmeans(
        pixels, k, None, criteria, 10, cv.KMEANS_RANDOM_CENTERS)
    # Convert centers from float to integer
    centers = np.uint8(centers)
    # Find the most predominant color
    dominant_color = centers[np.argmax(np.bincount(labels.flatten()))]

    # Debug
    # print("Dominant color (BGR):", dominant_color)
    # Create a square image with the dominant color
    color_image = np.zeros((5, 5, 3), np.uint8)
    color_image[:, :] = dominant_color
    return dominant_color, color_image


def get_image(image_path):
    img: cv.typing.MatLike = cv.imread(image_path)
    if img is None:
        raise Exception(f"No image at Location {image_path}")
    return img


def preprocess_image(img, gray=False, resize=True, crop=True, norm=False):
    if crop:
        img = img[10:]
    if resize:
        img = cv.resize(img, (512, 512))
    if gray:
        # Convert to grayscale if the model was trained on grayscale images
        img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    if norm:
        # Normalize pixel values to [0, 1]
        img = img / 255.0
    # Reshape the image to fit the model input (e.g., (1, 64, 64, 1) for a single grayscale image)
    # img = img.reshape(512, 512, 1)
        # return _grey, img
    return img


def get_and_preprocess_image(image_path: str, gray=False, resize=True, crop=True, norm=False) -> cv.typing.MatLike:
    return preprocess_image(get_image(image_path), gray, resize, crop, norm)
    # remove date


def load_image_paths_and_labels(base_path):
    images = []
    labels = []
    ignored = ['capture', 'data_old']
    for folder in [x for x in os.listdir(base_path) if x not in ignored]:
        folder_path = os.path.join(base_path, folder)
        for file in os.listdir(folder_path):
            img_path = os.path.join(folder_path, file)
            # img = preprocess_image(img_path)
            images.append(img_path)
            labels.append(folder)  # Assuming folder names are 0-9
    # labels = to_categorical(labels, num_classes=15)  # One-hot encode labels
    return images, labels


def get_value_roi(image):
    _image = preprocess_image(image, crop=False, resize=False, gray=True)
    # These values were obtained after testing
    return cv.resize(_image[80:225, 80:215], (150, 150)).reshape(150, 150, 1)


def find_center_contours(img, contours):
    image_center = np.asarray(img.shape) / 2

    final = []
    for contour in contours:
        # find center of each contour
        M = cv.moments(contour)
        center_X = int(M["m10"] / M["m00"])
        center_Y = int(M["m01"] / M["m00"])
        contour_center = (center_X, center_Y)

        # calculate distance to image_center
        distances_to_center = (euclidian_distance(
            image_center, contour_center))
        final.append({'contour': contour, 'center': contour_center,
                     'distance_to_center': distances_to_center})
    return sorted(final, key=lambda i: i['distance_to_center'])[:1]


def process_in_directory(folder_path, process, **kwargs):
    out = []
    for file in tqdm(os.listdir(folder_path)):
        img_path = os.path.join(folder_path, file)
        if img_path.endswith(('.png', '.jpg', '.jpeg')):
            out.append(process(get_image(img_path), **kwargs) + (img_path,))
    return out


def get_up(image_path):
    # Get the parent folder name containing the image
    parent_folder_name = os.path.basename(os.path.dirname(image_path))
    return parent_folder_name


if __name__ == '__main__':
    # final = cv.imread('./data/blue_0.png')
    # show(final)
    # img, _ = find_color(cv.imread('./WK22_Files_DeepLearning3/Unocardgreen1.jpg'))
    # show(img)
    # color_string(_)
    pass
